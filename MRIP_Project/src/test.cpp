/*
Author: Scott Franz

Test file for mzml db.
*/



#include "db.hpp"
#include "test_io.hpp"

#include <vector>
#include <limits>
#include <ctime>
#include <string>

#define NUM_TYPE unsigned short int

// ./mrip Sample_3_111227111659.txt window_test_2.txt 1073741824
// ./mrip Sample_3_111227111659.txt window_test_2.txt 536870912
// ./mrip Sample_3_111227111659.txt window_test_3.txt 536870912
// ./mrip Sample_3_111227111659.txt window_test_3.txt 1073741824
// ./mrip Sample_3_111227111659.txt window_test_3.txt 1610612736
// ./mrip Sample_3_111227111659.txt window_test_3.txt 0

int main(){
// int main(int argc,  char** argv){
    
	std::freopen("log.txt", "w", stdout);
	std::freopen("log.txt", "a", stdout);
	std::cout.precision(17);
	
	// if(argc != 4){
		// std::cerr << "ERROR: Incorrect format. Use './mrip <data file> <window file> <cache size in bytes>'" << std::endl;
	// }
	// else{
		// std::string fname_data = std::string(argv[1]);
		// std::string fname_window = std::string(argv[2]);
		// std::string cache_size_str = std::string(argv[3]);
		
		// std::vector<rtscan> rtscans;
		// test_io::read_example_file(fname_data, rtscans);
		
		// std::vector<std::array<double, 4>> windows;
		// test_io::read_test_windows(fname_window, windows);
		
		// std::size_t ram_cache_size = std::stol(cache_size_str);
		
		// db data_base(10, rtscans, ram_cache_size);
		// std::vector<dp> ilist;
		
		// std::cout << "[db::total_tables] " << data_base.total_tables << std::endl;
		
		// std::clock_t start_time = clock();
		// for(std::size_t i = 0; i < windows.size(); i++){
			// data_base.get_range(windows[i][0], windows[i][1], windows[i][2], windows[i][3], ilist);
		// }
		// std::clock_t finish_time = clock();
		
		// std::clock_t total_time = finish_time - start_time;
		// std::cout << "RESULTS: Totla time for set: " << total_time << std::endl;
		// std::cerr << "RESULTS: Totla time for set: " << total_time << std::endl;
	// }
	
	std::vector<rtscan> rtscans;
	test_io::read_example_file("Sample 3_111227111659.txt", rtscans);
    
	std::size_t num_points = 0;
	for(std::size_t i = 0; i < rtscans.size(); i++){
		num_points += rtscans.at(i).size();
	}
	std::cout << "num_points=" << num_points << std::endl;
	
	db data_base(10, rtscans, 100);
	
	std::vector<dp> ilist;
    
	std::clock_t start_time_1 = clock();
	data_base.get_range(data_base.mz_min, data_base.mz_max, data_base.rt_min, data_base.rt_max, ilist);
	std::clock_t finish_time_1 = clock();
	
	std::clock_t start_time_2 = clock();
	data_base.get_range(data_base.mz_min, data_base.mz_max, data_base.rt_min, data_base.rt_max, ilist);
	std::clock_t finish_time_2 = clock();
	
	std::cout << "run_time_1=" << (finish_time_1 - start_time_1) << std::endl;
	std::cout << "run_time_2=" << (finish_time_2 - start_time_2) << std::endl;
	
	return 0;
}