/*
Author: Scott Franz

Parent dependency for all other objects.
*/

#ifndef _DEPENDENCY_HPP
#define _DEPENDENCY_HPP

//Data point dependency.
#include "dp.hpp"
#include "rtscan.hpp"

#endif