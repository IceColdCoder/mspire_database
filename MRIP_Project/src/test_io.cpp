#include "test_io.hpp"

test_io::test_io(){}

test_io::~test_io(){}

void test_io::tokenize(std::string line, char delim, std::vector<std::string> & s_line){
	std::stringstream ss(line);
	std::string tok;
	
	while(std::getline(ss, tok, delim)){
		s_line.push_back(tok);
	}
}

void test_io::read_example_file(std::string file_name, std::vector<rtscan> & rtscans){
	std::ifstream file(file_name);
	std::string line;
	
	if(!file.is_open()){
		std::cerr << "ERROR: Failed to open test file '" << file_name << "'!" << std::endl;
		return;
	}
	
	int line_count = 0;
	while(std::getline(file, line)){
		line.pop_back();
		line.pop_back();
		std::vector<std::string> s_line;
		tokenize(line, '\t', s_line);
		
		/*Check for new rt scan.*/
		if(s_line.size() == 1){
			double rt = std::stod(s_line.at(0));
			rtscans.push_back(rtscan(rt));
		}
		/*Add data point to current rt scan.*/
		else{
			double mz = std::stod(s_line.at(0));
			double in = std::stod(s_line.at(1));
			rtscans.back().append(mz, in);
		}
		line_count++;
	}
	
	file.close();
	
	std::cout << "Successfully read file '" << file_name << "' with " << line_count << " lines!" << std::endl;
}

void test_io::read_test_windows(std::string file_name, std::vector<std::array<double, 4>> & windows){
	std::ifstream file(file_name);
	std::string line;
	
	if(!file.is_open()){
		std::cerr << "ERROR: Failed to open test file '" << file_name << "'!" << std::endl;
		return;
	}
	
	int line_count = 0;
	while(std::getline(file, line)){
		std::vector<std::string> s_line;
		tokenize(line, '\t', s_line);
		

		double mz_min = std::stod(s_line.at(0));
		double mz_max = std::stod(s_line.at(1));
		double rt_min = std::stod(s_line.at(2));
		double rt_max = std::stod(s_line.at(3));
		windows.push_back({mz_min, mz_max, rt_min, rt_max});
		
		line_count++;
	}
	
	file.close();
	
	std::cout << "Successfully read file '" << file_name << "' with " << line_count << " lines!" << std::endl;
}