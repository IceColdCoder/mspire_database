/*
Author: Scott Franz

Purpose: Main data base.
*/

#include "cache_table.hpp"

#include <algorithm>
#include <limits>

//Debug.
#include <string>
#include <sstream>
#include <iostream>
#include <queue>
#include <utility>

class db{
	private:
        //Number of items that can be contained in a HD data block.
        const std::size_t block_data_count = 8192 / sizeof(dp);
    
		//Comparator to check rt ranges.
		struct rt_less_than{
			bool operator()(std::vector<cache_table> & p, double s) const{ return p.at(0).rt_max < s; }
			bool operator()(double p, std::vector<cache_table> & s) const{ return p < s.at(0).rt_max ; }
		};
		
		//Comparator to check mz ranges.
		struct mz_less_than{
			bool operator()(cache_table & p, double s) const{ return p.mz_max < s; }
			bool operator()(double p, cache_table & s) const{ return p < s.mz_max; }
		};
		
		//Comparator for clock_t in priority_queue.
		struct clock_t_less_than{
			bool operator()(std::pair<cache_table*, std::clock_t> & a, std::pair<cache_table*, std::clock_t> & b) const{ return a.second < b.second; }
		};
		
		//Maximum size of cache contained in RAM determined by number of cache_tables. This is a soft limit.
		std::size_t num_ram_cache;
		
		//List of cache tables.
		std::vector<std::vector<cache_table>> tlist;
		
		//Priority Queue containing pointers to cache_tables and access times.
		std::priority_queue<std::pair<cache_table*, std::clock_t>, std::vector<std::pair<cache_table*, std::clock_t>>, clock_t_less_than> tqueue;
		
		//Method to get the total number of data points.
		std::size_t get_num_elements(std::vector<rtscan> & scans);
		
		//Method to get lowest mz from a list of rtscans.
		dp get_lowest_mz_index(std::vector<rtscan> & scans, std::vector<std::size_t> & scans_indecies);
		
		//Method to get lowest rt from a list of rtscans.
		double get_rt_interval(std::vector<rtscan> & scans);
		
		//Method to insert rtscans into the database.
		void insert(int set_id, std::vector<rtscan> & scans);
		
		//Method to check intersection.
		bool is_over_lapping(double low, double high, double lower_bound, double upper_bound);
		
		//Method to convert cache size in Bytes to number of presistent cache_blocks to keep in RAM.
		static std::size_t conv_byte_to_block_count(long bytes);
		
	public:
		double mz_min;
		double mz_max;
		double rt_min;
		double rt_max;
	
		db(std::size_t num_cuts, std::vector<rtscan> & scans, std::size_t size_ram_cache);
		virtual ~db();
		
		//Method to retrieve all data points that intersect with a given mz,rt range.
		void get_range(double mz_min, double mz_max, double rt_min, double rt_max, std::vector<dp> & ilist);
		
		//Method to trim cache.
		void trim();
		
		//Total number of cache_tables in this data_base.
		std::size_t total_tables;
};