#include "cache_block.hpp"

#define NUM_TYPE unsigned short int

#define NDEBUG
#include <cassert>

cache_block::cache_block(int set_id, int block_id):
num_data(0),
set_id(set_id),
block_id(block_id){
	cache_data.reserve(num_data); //Reserve space in temporary list.
	
	boost::filesystem::path p(get_directory()); //Get directory cache resides.
	
	//Creates cache directory if it exists.
	if(!boost::filesystem::exists(p)){
		boost::filesystem::create_directories(p);
	}
}

cache_block::~cache_block(){}

std::string cache_block::get_directory(){
	return "./../dpcache";
}

std::string cache_block::get_file_name(int set_id, int block_id){
	std::stringstream ss;
	ss << get_directory() << "/" << "dpcb_" << set_id << "_" << block_id << ".dpcache";
	return ss.str();
}

NUM_TYPE cache_block::insert(dp & p){
	cache_data.push_back(p); //Insert data point into list.
	return cache_data.size() - 1;
}

bool cache_block::write_cache(){
	std::string fname = get_file_name(set_id, block_id); //Get file name.
	
	boost::iostreams::mapped_file_params mparams(fname); //Parameters for memory mapped files.
	
	mparams.offset = 0; //Write offset.
	mparams.new_file_size = 8192; //Initial block size to allocate.
	
	boost::iostreams::mapped_file_sink mmap; //Cache file.
	
	mmap.open(mparams); //Open the cache file.
	
	//Check if the file was open.
	if(!mmap.is_open()){
		std::cerr << "ERROR: Failed to open mapped_file " << mparams.path << std::endl;
        
		return false; //FIXME. Potentially replace with throw error. Otherwise fine the way it is.
	}
	else{
		// std::cout << "File='" << fname << "' was opened successfully!" << std::endl;
	}
	
	char * current_head = mmap.data(); //Pointer to write head.
	long offset = 0; //Offset from write head.
	
    std::cout << "Cache_Data.size() = " << cache_data.size() << std::endl;
	for(unsigned int i = 0; i < cache_data.size(); i++){
		//Write data to memory.
        std::cout << "  Write " << i << ": (current_head, offset) = (" << current_head << " , " << offset << ")" << std::endl;
		memcpy(current_head + offset, &(cache_data.at(i)), DP_SIZE);
		
		//Increment write offset in cache.
		offset += DP_SIZE;
	}
	
	//Close mapped file.
	mmap.close();
	
	return true;
}

bool cache_block::read_cache(){
	std::string fname = get_file_name(set_id, block_id); //Get file name.
    
    if(!boost::filesystem::exists(fname)){
        std::cout << "[cache_block::read_cache]ERROR: " << fname << " does not exist." << std::endl;
        return false;
    }
    else{
        boost::iostreams::mapped_file_params mparams(fname); //Create parameters for memory mapped file.
        mparams.offset = 0; //Initial read/write offset.
        mparams.length = 8192; //Total size.
        boost::iostreams::mapped_file_source mmap; //Memory mapped file.
        mmap.open(mparams); //Open file.
        
        if(!mmap.is_open()){
            std::cout << "[cache_block::read_cache]ERROR: Failed to open " << fname << "." << std::endl;
            return false;
        }
        
        const char * file_current = mmap.data(); //Pointer to current location in file.
        
        //Retrieve all data from file.
        for(std::size_t i = 0; i < num_data; i++){
            long offset = i * DP_SIZE; //Current location of data to be retrieved.
            
            char data[DP_SIZE]; //Buffer to write into from file.
            
            memcpy(data, file_current + offset, DP_SIZE); //Read data from file.
            
            //Recast binary data as section.
            dp * p = reinterpret_cast<dp*>(data);
            
            //Constrct dp object.
            cache_data.push_back((*p));
        }
        
        //Close memory mapped file.
        mmap.close();
        assert(cache_data.size() != 0 && "[cache_block::read_cache]Error cache_data is empty.");
        return true;
    }
	return false;
}

bool cache_block::finalize(){	
	//First record the total number of stored data points.
	num_data = cache_data.size();
	
	//Second write data to cache.
	if(!write_cache()){
		std::cout << "[cache_block::finalize]ERROR Failed to write block=" << block_id << " to cache!" << std::endl;
		return false; //FIXME. Potentially replace with throw. Fine if otherwise.
	}
	
	//Finally, clear allocated list.
	trim();
	
	return true;
}

void cache_block::trim(){
	std::vector<dp> v;
	cache_data.clear();
	cache_data.swap(v);
}

bool cache_block::is_trimmed(){
	return cache_data.empty();
}

std::size_t cache_block::get_block_size(){
	return BLOCK_SIZE;
}