/*
Author: Scott Franz

Purpose: Block to represent a single data point of an mzml file.
*/

#ifndef _DP_HPP
#define _DP_HPP

//Debug.
#include <string>
#include <sstream>
#include <iostream>

#pragma pack(push, 1)
class dp{
	private:
	
	public:
		//Data.
		double mz;
		double rt;
		double in;
		
		dp(double mz, double rt, double in);
		virtual ~dp();
};
#pragma pack(pop)

#endif