#include "db.hpp"

#define NDEBUG
#include <cassert>

db::db(std::size_t num_cuts, std::vector<rtscan> & scans, std::size_t size_ram_cache):
num_ram_cache(db::conv_byte_to_block_count(size_ram_cache)),
mz_min(std::numeric_limits<double>::max()),
mz_max(std::numeric_limits<double>::min()),
rt_min(std::numeric_limits<double>::max()),
rt_max(std::numeric_limits<double>::min()),
total_tables(0){
    assert(scans.size() != 0);
	for(std::size_t i = 0; i < scans.size(); i+= num_cuts){
		std::vector<rtscan> scan_block;
		for(std::size_t j = 0; j < num_cuts; j++){
			if(i + j < scans.size()){
				scan_block.push_back(scans.at(i + j));
			}
		}
		
		insert(i, scan_block);
	}
	
	std::cout << num_ram_cache << std::endl;
}

db::~db(){
	
}

std::size_t db::get_num_elements(std::vector<rtscan> & scans){
	std::size_t num_elements = 0;
	for(std::size_t i = 0; i < scans.size(); i++){
		num_elements += scans.at(i).size();
	}
	return num_elements;
}

dp db::get_lowest_mz_index(std::vector<rtscan> & scans, std::vector<std::size_t> & scans_indecies){
	std::size_t lowest_index = 0; //Index of the lowest mz.
	double lowest_mz = std::numeric_limits<double>::max(); //Lowest mz found.
	
	//Iterate over all rt scans.
	for(std::size_t i = 0; i < scans.size(); i++){
		//If index is valid.
		if(scans_indecies.at(i) < scans.at(i).size()){
			std::size_t cindex = scans_indecies.at(i); //Get current lowest index of this mz.
			double tmp_mz = scans.at(i).at(cindex).mz; //Temporary mz.
			
			//Check if this mz is the lowest.
			if(tmp_mz < lowest_mz){
				lowest_mz = tmp_mz;
				lowest_index = i;
			}
		}
	}
	
	//Increment the index of the rtscan that had the lowest mz value.
	scans_indecies.at(lowest_index)++;
	//Return data point.
	return scans.at(lowest_index).at(scans_indecies.at(lowest_index) - 1);
}

double db::get_rt_interval(std::vector<rtscan> & scans){
	double rt_prev = scans.at(0).rt; //Previous rt scan.
	double rt_interval = std::numeric_limits<double>::max(); //rt interval to be returned.
	
	for(std::size_t i = 1; i < scans.size(); i++){
		double rt_next = scans.at(i).rt; //Next rt.
		
		double tmp_interval = rt_next - rt_prev; //Interval between the next rt and the previous.
		
		//Check if new interval is smallest. If so replace.
		if(tmp_interval < rt_interval){
			rt_interval = tmp_interval;
		}
	}
	
	return rt_interval;
}

void db::insert(int set_id, std::vector<rtscan> & scans){
	
	//Push new list of caches for this set of rtscans onto list.
	tlist.push_back(std::vector<cache_table>());

	//Get the rt interval.
	double rt_interval = get_rt_interval(scans);
	
	//Get the total number of elements.
	std::size_t num_points = get_num_elements(scans);
	
	//Primary index list.
	std::vector<std::size_t> scans_indecies(scans.size(), 0);
	
	//For every block.
	for(std::size_t i = 0; i < num_points; i += block_data_count){
		//List of data points to be inserted into a cache_block.
		std::vector<dp> block_list;
		
		//mz interval.
		double mz_interval = std::numeric_limits<double>::max();
		
		//Previous mz interval with initial value.
		double mz_prev = -1;
		
		//For every point within the block.
		for(std::size_t j = 0; j < block_data_count; j++){
			//Get lowest mz data point.
			if(i + j < num_points){
				//Get lowest index.
				dp low_p_index = get_lowest_mz_index(scans, scans_indecies);
//				std::cout << "[db::insert] Lowest= " << low_p_index.mz << std::endl;
				
				//If this is not the first index.
				if(mz_prev > -1){
					double tmp_mz_interval = low_p_index.mz - mz_prev; //Get the current mz interval.
                    std::cout << "      mz_interval= " << tmp_mz_interval << std::endl;
					
					//Check interval.
					if(tmp_mz_interval < mz_interval && tmp_mz_interval > 0){
						mz_interval = tmp_mz_interval;
					}
				}

				//Set mz prev to next mz.
				mz_prev = low_p_index.mz;				
				
				//Insert the data point into a list within this block.
				block_list.push_back(low_p_index);
				
				total_tables++;
			}
		}
		
		//Instaniate cache.
		double mz_low = block_list.at(0).mz;
		double mz_high = block_list.at(block_list.size() - 1).mz;
		double rt_low = scans.at(0).rt;
		double rt_high = scans.at(scans.size() - 1).rt;
		
		//Check global boundries.
		if(mz_low < mz_min){
			mz_min = mz_low;
		}
		if(mz_high > mz_max){
			mz_max = mz_high;
		}
		if(rt_low < rt_min){
			rt_min = rt_low;
		}
		if(rt_high > rt_max){
			rt_max = rt_high;
		}
		
		//Push back a new clash table.
		tlist.back().push_back(cache_table(set_id / 10, i / block_data_count, mz_low, mz_high, rt_low, rt_high, mz_interval, rt_interval, block_list, std::clock()));
	}
}

bool db::is_over_lapping(double low, double high, double lower_bound, double upper_bound){
	return lower_bound <= high && low <= upper_bound;
}

std::size_t db::conv_byte_to_block_count(long bytes){
	//Get and convert cache_block size to bytes.
	return bytes / (cache_block::get_block_size() / 8);
}

void db::get_range(double mz_min, double mz_max, double rt_min, double rt_max, std::vector<dp> & ilist){	
	//Get valid rt ranges. //OPTIMIZE
	auto rt_low = std::lower_bound(tlist.begin(), tlist.end(), rt_min, rt_less_than());
	
	//Iterate from low end until out of range or end of list.
	for(auto a = rt_low; (a != tlist.end() && (*(*a).begin()).rt_max < rt_max); a++){
		//Get valid mz ranges. //OPTIMIZE
		auto mz_low = std::lower_bound((*a).begin(), (*a).end(), mz_min, mz_less_than());
                          
		for(auto b = mz_low; (b != (*a).end() && (*(*a).rbegin()).mz_max < mz_max); b++){
			std::clock_t access_time = std::clock();
			(*b).get(mz_min, mz_max, rt_min, rt_max, ilist, access_time);
			tqueue.push(std::make_pair(&(*b), access_time));
			trim();
		}
	}
}

void db::trim(){
	if(tqueue.size() > num_ram_cache){
		auto ct_ptr = tqueue.top();
		while(ct_ptr.second != ct_ptr.first->access_time){
			tqueue.pop();
			ct_ptr = tqueue.top();
		}
		if(tqueue.size() > num_ram_cache){
					tqueue.pop();
		ct_ptr.first->trim();
		}
	}
}