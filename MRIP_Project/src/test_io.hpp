/*
Author: Scott Franz

Purpose: To read in example test data to test this data base.
*/

#ifndef _TEST_IO_HPP
#define _TEST_IO_HPP

#include "dependency.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <array>
#include <limits>

class test_io{
	private:
		test_io();
		virtual ~test_io();
		
		//Method to tokenize example file.
		static void tokenize(std::string line, char delim, std::vector<std::string> & s_line);

	public:
		//Method to read external example file derived from mzml file.
		static void read_example_file(std::string file_name, std::vector<rtscan> & rtscans);
		
		//Method to read in a series of windows from a file.
		static void read_test_windows(std::string file_name, std::vector<std::array<double, 4>> & windows);
};

#endif