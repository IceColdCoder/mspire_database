/*
Author: Scott Franz

Purpose: To handle I/O involved with caching mzml data.
*/

#ifndef _CACHE_BLOCK_HPP
#define _CACHE_BLOCK_HPP

//Global dependencies.
#include "dependency.hpp"

//Local dependencies.
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream> //For opening a file.
#include <boost/filesystem.hpp> //For getting directory information.
#include <boost/iostreams/device/mapped_file.hpp> //For cache.

#define NUM_TYPE unsigned short int

class cache_block{
	private:
		//Size of the section to be written into cache.
		static const std::size_t DP_SIZE = sizeof(dp);
	
		//Ideal size of cache on HD.
		static const std::size_t BLOCK_SIZE = 8192 * 8; //8192 * 8 bits.
		
		//List of data points stored in block before finalization.
//		std::vector<dp> cache_data;
		
		//Method to return cache directory.
		static std::string get_directory();
		//Method to return file name of cache_block from block id.
		static std::string get_file_name(int set_id, int block_id);
		
		//Number of data points stored in this cache block.
		std::size_t num_data = 8192 / sizeof(dp);
		
		//Internal method to temperary list into cache.
		bool write_cache();
	
	public:
		//Set ID of this cache block.
		int set_id;
	
		//Block ID of this cache block.
		int block_id;
		
		//Cache represented in RAM.
		std::vector<dp> cache_data;
	
		cache_block(int set_id, int block_id);
		virtual ~cache_block();
		
		//Method to insert data point into a temporary list for writing upon finalization.
		NUM_TYPE insert(dp & p);
		
		//Method to read data from given index list from this cache into collection of queried data points.
		bool read_cache();
		
		//Method to finalize this cache block onto HD and clear any temporary memory allocated while building it.
		bool finalize();
		
		//Method to trim cache.
		void trim();
		
		//Check to see if cache has been trimmed.
		bool is_trimmed();
		
		//Method get block size in bits.
		static std::size_t get_block_size();
};

#endif