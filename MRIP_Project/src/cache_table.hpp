/*
Author: Scott Franz
in
Purpose: To provide a rapid recall of indexded data  RAM for the contents in cache.
*/

#ifndef _CACHE_TABLE_HPP
#define _CACHE_TABLE_HPP

#include "cache_block.hpp"

//Dependencies.
#include <algorithm>
#include <utility>
#include <limits>
#include <ctime>

//Debug.
#include <string>
#include <sstream>
#include <iomanip>

#define NUM_TYPE unsigned short int

class cache_table{
	private:
		//Struct to contain comparison operators for binary search.
		struct comp_less_than{
			bool operator()(std::pair<NUM_TYPE, unsigned short int> & p, NUM_TYPE s) const{ return p.first < s; }
			bool operator()(NUM_TYPE s, std::pair<NUM_TYPE, unsigned short int> & p) const{ return s < p.first; }
		};
		
		//Main lookup table for indexing data.
		std::vector<std::pair<NUM_TYPE, unsigned short int>> lookup_table;
		
		//Cache that this table corrisponds to.
		cache_block cb;
		
		//Interval over which this table is defined.
		double mz_interval;
		double rt_interval;
		
		//Number of points inserted into this table.
		std::size_t inserted_count;
		
		//Size of rt_array as defined by min rt_interval.
		long mz_array_size;
		long rt_array_size;
		
		//Method to determine wether a given window intersects with this table.
		static bool is_over_lapping(double value, double lower_bound, double upper_bound);
		
		//Function to map a 2 dimensional array to a 1 dimensional array.
		static long map_2d_to_1d(long row, long col, long width);
		
		//Function to map a 1 dimensional array to the x-axis of a 2 dimensional array.
		static long map_to_2d_x(long index, long vertical_size);
		
		//Functiont to map a 1 dimensional array to the y-axis of a 2 dimensional array.
		static long map_to_2d_y(long index, long vertical_size);
		
		//Function go get the hash value of this value on the mz, rt, in axis.
		static unsigned short get_hash(double val, double val_min, double val_interval);
		
		//Method to insert data point node into table.
		bool insert(dp & p);
		
	public:
		//Bounds over which this table is defined.
		double mz_min;
		double mz_max;
		double rt_min;
		double rt_max;
		
		//Var to track the last time this table accessed.
		std::clock_t access_time;
		
		cache_table(int set_id, int block_id, double mz_min, double mz_max, double rt_min, double rt_max, double mz_interval, double rt_interval, std::vector<dp> & A, std::clock_t access_time);
		virtual ~cache_table();
		
		void get(double mz_low, double mz_high, double rt_low, double rt_high, std::vector<dp> & ilist, std::clock_t access_time);
		
		std::string to_string();
		
		//Number of data points stored in this cache.
		std::size_t size();
		
		//Method to trim this cache of cache in RAM. Cache in HD remains.
		void trim();
};

#endif