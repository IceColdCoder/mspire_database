/*
Author: Scott Franz

Purpose: Contain and handle data from a single rt scan.
*/

#ifndef _RTSCAN_HPP
#define _RTSCAN_HPP


#include "dp.hpp"

#include <vector>

//Debug.
#include <iostream>
#include <sstream>
#include <string>

class rtscan{
	private:
		//Data contained within this can.
		std::vector<dp> scan;
	
	public:
		//Time signature of this rtscan.
		double rt;
	
		rtscan(double rt);
		virtual ~rtscan();
		
		//Append a data point to this rtscan.
		void append(dp p);
		
		//Append a data point with mz & intensity values.
		void append(double mz, double in);
		
		//Get lowest mz data point from this list and keep track of the current index.
		dp at(std::size_t current);
		
		//Returns the numer of data points contained in this rtscan.
		std::size_t size();
		
		//Debug. Return string representation of this rtscan.
		std::string to_string();
};

#endif