#include "cache_table.hpp"

#define DEBUG
#include <cassert>

#define NUM_TYPE unsigned short int

cache_table::cache_table(int set_id, int block_id, double mz_min, double mz_max, double rt_min, double rt_max, double mz_interval, double rt_interval, std::vector<dp> & A, std::clock_t access_time):
cb(set_id, block_id),
mz_interval(mz_interval),
rt_interval(rt_interval),
inserted_count(0),
mz_min(mz_min),
mz_max(mz_max),
rt_min(rt_min),
rt_max(rt_max),
access_time(access_time){
	
	mz_array_size = ((mz_max - mz_min) / mz_interval) + 1;
	rt_array_size = ((rt_max - rt_min) / rt_interval) + 1;
	
	for(std::size_t a = 0; a < A.size(); a++){
		insert(A.at(a));
	}
	
	cb.finalize();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
}

cache_table::~cache_table(){}

bool cache_table::is_over_lapping(double value, double lower_bound, double upper_bound){
	return lower_bound <= value && value <= upper_bound;
}

long cache_table::map_2d_to_1d(long row, long col, long width){
	return (width * row) + col;
}
		
long cache_table::map_to_2d_x(long index, long vertical_size){
	return index / vertical_size;
}
		
long cache_table::map_to_2d_y(long index, long vertical_size){
	return index % vertical_size;
}

unsigned short cache_table::get_hash(double val, double val_min, double val_interval){
    //Fix attempted to ensure there are no hash collissions.
    double h_val = val / val_interval;
    double h_val_min = val_min / val_interval;
    std::cout << "[val,val_min,val_interval,h_val,h_val_min,sum] = [" << val << "," << val_min << "," << val_interval << "," << h_val << "," << h_val_min << "," << (h_val - h_val_min) << "]" << std::endl;
    assert(h_val - h_val_min < 65536);
   
	return h_val - h_val_min;
}

bool cache_table::insert(dp & p){
	unsigned short int mz_hash = get_hash(p.mz, mz_min, mz_interval); //Hash on mz axis.
	unsigned short int rt_hash = get_hash(p.rt, rt_min, rt_interval); //Hash on rt axis.
	unsigned short int hash = map_2d_to_1d(rt_hash, mz_hash, mz_array_size); //Final hash key for this data point.
	
	//Debug. Binary search for data hash before insert to check if there is a collision.
	assert((std::binary_search(lookup_table.begin(), lookup_table.end(), hash, comp_less_than()) == false));
        // {
		// std::cout << "[cache_table::insert]ERROR: insert hash=" << hash << " was found." << std::endl;
		// return false;
	// }
	
	unsigned short int key = cb.insert(p);
	
	//Point to be inserted into the data base.
	std::pair<unsigned short, unsigned short int> item = std::make_pair(hash, key);
	
	//Insert the point into the table.
	lookup_table.insert(std::upper_bound(lookup_table.begin(), lookup_table.end(), item), item);
	
	return true;
}

void cache_table::get(double mz_low, double mz_high, double rt_low, double rt_high, std::vector<dp> & ilist, std::clock_t access_time){
	assert(lookup_table.size() == cb.cache_data.size());
    
    //Update current access time.
	this->access_time = access_time;
	
	//Index list to pass into function.
	std::vector<unsigned short> index_list;

	//Case where this cache block is escased by the bounds of the window.
	if(mz_low < mz_min && mz_max < mz_high && rt_low < rt_min && rt_max < rt_high){
		
		//If the cache has been trimmed then it needs to be reread.
		if(cb.is_trimmed()){
			//Read data from cache.
			cb.read_cache();
		}
		
		//Add this cache_data into ilist.
		ilist.insert(ilist.end(), cb.cache_data.begin(), cb.cache_data.end()); 
	}
	//Case where this cache block does not completely overlap bounds provided.
	else{
		//Normalize the window to this cache.
		if(mz_low < mz_min){
			mz_low = mz_min;
		}
		if(mz_max < mz_high){
			mz_high = mz_max;
		}
		if(rt_low < rt_min){
			rt_low = rt_min;
		}
		if(rt_max < rt_high){
			rt_high = rt_max;
		}
		
		//Get valid hash ranges for this window.
		unsigned short mz_hash_min = get_hash(mz_low, mz_min, mz_interval);
		unsigned short mz_hash_max = get_hash(mz_high, mz_max, mz_interval);
		unsigned short rt_hash_min = get_hash(rt_low, rt_min, rt_interval);
		unsigned short rt_hash_max = get_hash(mz_high, rt_max, rt_interval);
		
		unsigned short hash_min = map_2d_to_1d(rt_hash_min, mz_hash_min, mz_array_size);
		unsigned short hash_max = map_2d_to_1d(rt_hash_max, mz_hash_max, mz_array_size);
		
		//Get iterators within valid constraints. //OPTIMIZE
		auto low = std::lower_bound(lookup_table.begin(), lookup_table.end(), hash_min, comp_less_than());
		auto high = std::upper_bound(lookup_table.begin(), lookup_table.end(), hash_max, comp_less_than());
        
        std::cout << "<|lookup_table|,|cache_block|> = <" << lookup_table.size() << "," << cb.cache_data.size() << ">" << std::endl;
		
		//If the cache has been trimmed then it needs to be reread.
		if(cb.is_trimmed()){
			//Read data from cache.
			cb.read_cache();
		}
		
		//For every valid index within this table.
		for(auto it = low; it != high; ++it){
			ilist.push_back(cb.cache_data[(*it).second]);
		}
	}
}

std::string cache_table::to_string(){
	std::stringstream ss;
	ss << "{cache_table::(" << std::setprecision(17) << mz_min << "," << std::setprecision(17) << mz_max << "," << std::setprecision(17) << mz_interval << ")U(" << std::setprecision(17) << rt_min << "," << std::setprecision(17) << rt_max << "," << std::setprecision(17) << rt_interval << ")}";
	return ss.str();
}

std::size_t cache_table::size(){
	return lookup_table.size();
}

void cache_table::trim(){
	if(!cb.is_trimmed()){
		cb.trim();
	}
}