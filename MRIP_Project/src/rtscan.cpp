#include "rtscan.hpp"

rtscan::rtscan(double rt):
rt(rt){
	
}

rtscan::~rtscan(){}

void rtscan::append(dp p){
	scan.push_back(p);
}

void rtscan::append(double mz, double in){
	scan.push_back(dp(mz, rt, in));
}

dp rtscan::at(std::size_t current){
	return scan.at(current);
}

std::size_t rtscan::size(){
	return scan.size();
}

std::string rtscan::to_string(){
	std::stringstream ss;
	ss << "{";
	for(std::size_t i = 0; i < scan.size(); i++){
		ss << "(" << scan.at(i).mz << "," << scan.at(i).rt << "," << scan.at(i).in << ")";
		if(i < scan.size() - 1){
			ss << ", ";
		}
	}
	ss << "}";
	return ss.str();
}