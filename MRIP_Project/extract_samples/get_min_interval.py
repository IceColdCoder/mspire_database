#Author Scott Franz

import os
import sys


if __name__=="__main__":
  with open("Sample 3_111227111659.txt_out", "r") as file:
    S = []
    for line in file:
      A = line.split("\t")
      if(len(A) == 2):
        S.append(float(A[0]))
    
    S.sort()
    D = []
    for i in range(0, len(S) - 1):
      D.append(abs(S[i + 1] - S[i]))
      
    D.sort()
    D = list(set(D))
    print(D[:10])