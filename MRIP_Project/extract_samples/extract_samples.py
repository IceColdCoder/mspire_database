import os
import sys

class extract_samples:
    'Class to extract sample cases from text versions of mzml files.'
    
    def __init__(self, fname):
        self.fname = fname
        self.data = []
        
    def is_rt_scan(self, line):
        A = line.split(" ")
        
        #Case where line contains rt value.
        if(len(A) == 18):
            if(A[12] == "cvParam:" and A[13] == "scan" and A[14] == "start" and A[15] == "time,"):
                return True
        return False
        
    def get_rt(self, line):
        A = line.split(" ")
        a = A[16]
        return a[:-1]
        
    def is_binary(self, line):
        A = line.split(" ")
        
        if(len(A) > 10):
            if(A[10] == "binary:"):
                return True
        return False
        
    def get_binary(self, line):
        A = line.split(" ")
        
        B = []
        for n in range(12, len(A) - 1):
            B.append(A[n])
        return B

    def extract(self):
        stack_set = []
        with open(self.fname) as file:
            for line in file:
                if(self.is_rt_scan(line)):
                    if stack_set:
                        self.data.append(stack_set)
                    stack_set = []
                    stack_set.append(self.get_rt(line))
                elif(self.is_binary(line)):
                    stack_set.append(self.get_binary(line))
        
    def write_file(self):
        outfilename = self.fname + "_out"
        if os.path.exists(outfilename):
            os.remove(outfilename)
            
        with open(outfilename, "w") as file:
            for item in self.data:
                file.write(item[0] + "\n")
                if(len(item[1]) != len(item[2])):
                    system.exit("ERROR: length of mz & intensity list did not match! Prog terminating!")
                
                for i in range(len(item[1])):
                    file.write(item[1][i] + "\t" + item[2][i] + "\n")
                
if __name__ == "__main__":
    print(sys.argv)
    filename = "Sample 3_111227111659.txt"
    es = extract_samples(filename)
    data = es.extract()
    es.write_file()
    print("Program successful!")