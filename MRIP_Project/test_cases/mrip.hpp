#ifndef _MRIP_HPP
#define _MRIP_HPP

#pragma pack(push, 1)
struct MRIP{
	double mz;
	double rt;
	double in;
};
#pragma pack(pop)

#endif /* _MRIP_HPP */