/*
* Prog to generate test data for MRIP.
*
* Author: Scott Franz 041116
*
*
*/


#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <random>
#include <limits>
#include <cmath>
#include <fstream>

#include "mrip.hpp"

using namespace std;

void create_test(size_t num_nodes, std::vector<MRIP> & input_list, double mz_lower_bound, double mz_upper_bound, double rt_lower_bound, double rt_upper_bound, double in_lower_bound, double in_upper_bound);
void write_file(std::vector<MRIP> & input_list, std::string fname);

int main(int argc, char **argv){
	
	if(argc == 3){
		double mz_lower_bound = 0;
		double mz_upper_bound = 100;
		double rt_lower_bound = 0;
		double rt_upper_bound = 100;
		double in_lower_bound = 1;
		double in_upper_bound = 500;
	
		string num_data_str(argv[1]);
		int num_nodes = stoi(num_data_str);
		cout << "Creating test with " << num_nodes << " items..." << endl;
		
		vector<MRIP> input_list;
		create_test(num_nodes, input_list, mz_lower_bound, mz_upper_bound, rt_lower_bound, rt_upper_bound, in_lower_bound, in_upper_bound);
		
		string fname(argv[2]);
		write_file(input_list, fname);
	}
	else{
		cout << "ERROR: Format is './gt <num_items> <output_file_name>'." << endl;
	}
	
	return 0;
}

void create_test(size_t num_nodes, std::vector<MRIP> & input_list, double mz_lower_bound, double mz_upper_bound, double rt_lower_bound, double rt_upper_bound, double in_lower_bound, double in_upper_bound){
	uniform_real_distribution<double> mz_rand(mz_lower_bound, mz_upper_bound);
	uniform_real_distribution<double> rt_rand(rt_lower_bound, rt_upper_bound);
	uniform_real_distribution<double> in_rand(in_lower_bound, in_upper_bound);
	default_random_engine re;
	
	for(size_t i = 0; i < num_nodes; i++){

	MRIP mrip;
	mrip.mz = mz_rand(re);
	mrip.rt = rt_rand(re);
	mrip.in = in_rand(re);
	  
	input_list.push_back(mrip);
	  
	//cout << "MRIP(" << mrip.mz << "," << mrip.rt << "," << mrip.in << ")" << endl;
	}
}

void write_file(std::vector<MRIP> & input_list, std::string fname){
	ofstream file(fname);
	
	if(!file.is_open()){
		cerr << "ERROR: Failed to open file '" << fname << "'." << endl;
		exit(1);
	}
	
	for(size_t i = 0; i < input_list.size(); i++){
		file << input_list.at(i).mz << "," << input_list.at(i).rt << "," << input_list.at(i).in << "\n";
	}
	
	file.close();
}